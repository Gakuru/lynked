import { fork, all } from 'redux-saga/effects';

import landingPageCategoriesSaga from './src/widgets/landingPageCategories/LandingPageCategories.sagas';
import landingPageProductsSaga from './src/widgets/landingPageProducts/LandingPageProducts.sagas';
import shopProductsSaga from './src/widgets/shop/products/shopProducts.sagas';
import shops from './src/widgets/shops/shops.sagas';
import productOverviewSaga from './src/screens/products/product.sagas';
import userSaga from './src/screens/auth/user.sagas';

export default function* rootSaga() {
    yield all(
        [
            fork(landingPageCategoriesSaga),
            fork(landingPageProductsSaga),
            fork(shopProductsSaga),
            fork(shops),
            fork(productOverviewSaga),
            fork(userSaga)
        ]
    );

}