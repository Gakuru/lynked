/**
 * @format
 */

import 'react-native';
import React from 'react';
// import App from '../App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

import LandingPageProductsWidget from '../src/widgets/landingPageProducts/LandingPageProducts.widget';

import { Provider } from 'react-redux';
import store from '../store';

fetch = jest.fn(() => Promise.resolve());

// it('renders correctly', () => {
//   renderer.create(<App />);
// });

describe('Get feature products', () => {
  it('should pass if it returns a list for the featured products', async () => {
    renderer.create(
      <Provider store={store}>
        <LandingPageProductsWidget />
      </Provider>
    )
  });
});