
import Realm from 'realm';

const userSchema = {
    name: 'User',
    primaryKey: 'OAuthId',
    properties: {
        id: 'string',
        OAuthId: 'string',
        firstName: 'string',
        lastName: 'string',
        name: 'string',
        email: 'string',
        photoURI: 'string',
        OAuth: 'string',
        deviceToken: 'string',
    }
}

export const DBSchema = new Realm({ schema: [userSchema], schemaVersion: 0 });