import React from 'react';
import { Platform } from 'react-native';
import ImagePicker from 'react-native-image-picker';

export const ImageChooser = () => {
    console.log('foo bar');
    const options = {
        noData: true,
    }
    ImagePicker.launchImageLibrary(options, response => {
        if (response.uri) {
            return {
                // id: (product.images.length + 1).toString(),
                name: response.fileName,
                type: response.type,
                uri: Platform.OS === "android" ? response.uri : response.uri.replace("file://", ""),
                featured: true
            };
        } else {
            return null;
        }
    })
}