/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @flow
 * @format
 */

'use strict';

export default {
  primary: '#1292B4',
  white: '#FFF',
  lighter: '#F3F3F3',
  faded: '#F2F2F2',
  light: '#DAE1E7',
  dark: '#444444',
  black: '#000000',
  blue: '#25A7DB',

  lightTintColor: '#FFF1F1',
  fadedTintColor: '#FF4747',

  fadedGray: '#999999',
  darkGray: '#6e6e6e',

  inactiveTintColor: '#3A3E4A',
  activeTintColor: '#fc494d',


  // Brand colors
  googleRed: '#DE5246',
  facebookBlue: '#3B5998',

};
