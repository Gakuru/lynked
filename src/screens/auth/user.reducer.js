import { LOGIN_USER_SUCCESS } from "./user.types";

export const user = (state = {}, { type, user }) => {
    switch (type) {
        case LOGIN_USER_SUCCESS:
            return user;

        default:
            return state;
    }
}