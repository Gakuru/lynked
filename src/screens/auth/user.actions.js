import { REQUEST_LOGIN_USER, LOGIN_USER_SUCCESS } from "./user.types"

export const requestLoginUser = user => ({ type: REQUEST_LOGIN_USER, user });
export const loginUserSuccess = user => ({ type: LOGIN_USER_SUCCESS, user });