import React, { useState, useEffect } from 'react';
import { View, Text, StatusBar, SafeAreaView } from 'react-native';
import { Button } from 'react-native-elements';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';

import colors from '../../styles/colors.default';

import { DBSchema } from '../../schema/database.schema';

import RNFirebase from 'react-native-firebase';

import { requestLoginUser } from './user.actions';

const configurationOptions = {
    debug: true,
    promptOnMissingPlayServices: true
}

const firebase = RNFirebase.initializeApp(configurationOptions);

const OAuthMethods = {
    GOOGLE: 'GOOGLE',
    FACEBOOK: 'FACEBOOK'
}

const googleSiginConfig = () => {
    GoogleSignin.configure({
        scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
        webClientId: '791368172375-nd67cbou24jh9o00fc76em0ln2cr0h1s.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
        offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        hostedDomain: '', // specifies a hosted domain restriction
        loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
        forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
        accountName: '', // [Android] specifies an account name on the device that should be used
        // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    })
}

let userInfo = null;
let oAuthProfile = null;
let realm = null;
let loggedIn = false;
let deviceToken = '';

class LoginScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            user: null,
            realm: null,
            loggedIn: false
        }
    }

    componentDidMount() {
        const user = DBSchema.objects('User').filtered('id != ""')[0];

        firebase.messaging().getToken().then((token) => {
            console.log('token: ', token);
            deviceToken = token;
        });

        firebase.messaging().onTokenRefresh((token) => {
            console.log('refresh token: ', token);
            deviceToken = token;
        });

        if (user) {

            this.setState({ user });

            this.props.navigation.navigate('App');

        } else {
            console.log('handle login');
        }
    }

    componentDidUpdate(previousProps) {
        if (previousProps !== this.props.user) {
            let user = DBSchema.objects('User');
            DBSchema.write(() => {
                // Update the Realm local storage with the user id
                user[0].id = this.props.user.id;

                loggedIn = true;
                this.props.navigation.navigate('App');
            })
        }
    }

    handleLogin = async ({ OAuth }) => {
        switch (OAuth) {
            case OAuthMethods.GOOGLE:
                googleSiginConfig();
                try {
                    await GoogleSignin.hasPlayServices();
                    oAuthProfile = await GoogleSignin.signIn();
                    userInfo = oAuthProfile.user;
                    const { id, email, givenName: firstName, familyName: lastName, name, photo: photoURI } = userInfo;
                    DBSchema.write(() => {
                        realm = DBSchema.create('User', { id, OAuthId: id, firstName, lastName, name, email, photoURI, OAuth, deviceToken }, true);
                        console.log(deviceToken);
                        //Persist the user to the database
                        this.props.requestLoginUser({ OAuthId: id, first_name: firstName, last_name: lastName, full_name: name, email, photo_uri: photoURI, OAuth });
                    })
                } catch (error) {
                    console.log(error);
                    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                        // user cancelled the login flow
                        console.log(error.code)
                    } else if (error.code === statusCodes.IN_PROGRESS) {
                        // operation (e.g. sign in) is in progress already
                        console.log(error.code);
                    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                        // play services not available or outdated
                        console.log(error.code)
                    } else {
                        // some other error happened
                        console.log(error);
                        console.log('unkown error occured');
                    }
                }
                break;
            default:
                break;
        }
    }

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'flex-end', backgroundColor: colors.blue }}>
                <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
                <View style={{ marginBottom: 80 }}>

                    <View style={{ alignItems: 'center', marginBottom: 20 }}>
                        <Text style={{ fontSize: 32, fontWeight: 'bold' }}>Login with</Text>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                        <Button
                            buttonStyle={{ backgroundColor: colors.googleRed, borderRadius: 30 }}
                            onPress={(e) => this.handleLogin({ OAuth: OAuthMethods.GOOGLE })}
                            icon={{
                                type: 'antdesign',
                                name: "google",
                                size: 28,
                                color: colors.white
                            }}
                            title={'Google'}
                        />
                        <Button
                            buttonStyle={{ backgroundColor: colors.facebookBlue, borderRadius: 30 }}
                            onPress={(e) => this.handleLogin({ OAuth: OAuthMethods.FACEBOOK })}
                            icon={{
                                type: 'antdesign',
                                name: "facebook-square",
                                size: 28,
                                color: colors.white
                            }}
                            title={'Facebook'}
                        />
                    </View>

                </View>

            </View >
        )
    }
}

const mapStateToProps = (store) => {
    return {
        user: store.user
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestLoginUser
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
