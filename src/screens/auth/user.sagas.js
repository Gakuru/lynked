import { put, call, takeEvery, all } from 'redux-saga/effects';
import { REQUEST_LOGIN_USER } from './user.types';
import { loginUserSuccess } from './user.actions';
import { baseURL } from '../../api/url';

const _requestLoginUser = async ({ user }) => {
    try {
        const response = await fetch(`${baseURL}user`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': '',
            },
            body: JSON.stringify(user)
        });
        return await response ? response.json() : null;
    } catch (e) {
        console.log(e);
    }
}

function* requestLoginUser(params) {
    try {
        const data = yield call(_requestLoginUser, params);
        yield put(loginUserSuccess(data.user));
    } catch (err) {
        console.log(err);
    }
}

export default function* userSaga() {
    yield all([
        takeEvery(REQUEST_LOGIN_USER, requestLoginUser),
    ]);
}