import React, { Fragment } from 'react';

import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from 'react-native/Libraries/NewAppScreen/components/Colors';

const links = [
    {
        title: 'Unpaid',
        screen: 'unpaid',
        description: ''
    },
    {
        title: 'To be shipped',
        screen: 'earnings',
        description: ''
    },
    {
        title: 'Shipped',
        screen: 'earnings',
        description: ''
    },
];

export default class SellerOrdersScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Your orders',
        }
    };

    renderSeparator = index => {
        if (index < links.length - 1)
            return (
                <View style={styles.separator} />
            )
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    links.map((link, index) => {
                        return (
                            <Fragment key={index}>
                                <TouchableOpacity
                                    accessibilityRole={'button'}
                                    onPress={() => this.props.navigation.navigate(link.screen)}
                                    style={styles.linkContainer}>
                                    <Text style={styles.link}>{link.title}</Text>
                                    <Text style={styles.description}>{link.description}</Text>
                                </TouchableOpacity>
                                {this.renderSeparator(index)}
                            </Fragment>
                        )
                    })
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 10
    },
    linkContainer: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 8,
        paddingLeft: 5
    },
    link: {
        flex: 2,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.primary,
    },
    description: {
        flex: 3,
        paddingVertical: 16,
        fontWeight: '400',
        fontSize: 18,
        color: Colors.dark,
    },
    separator: {
        backgroundColor: Colors.light,
        height: 1,
    },
});