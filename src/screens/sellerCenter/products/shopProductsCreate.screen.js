import React, { Fragment } from 'react';

import { Platform, Dimensions } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { View, Text, TouchableOpacity, ScrollView, FlatList, Image, Picker, Switch } from 'react-native';
import { Button, Icon, Input } from 'react-native-elements';

import RNPickerSelect from 'react-native-picker-select';

import ImagePicker from 'react-native-image-picker';

import styles from '../../../styles/style.default';

import { requestCreateShopProduct } from '../../../widgets/shop/products/shopProducts.actions';

import Colors from '../../../styles/colors.default';
import ShopHeaderWidget from '../../../widgets/shop/header/shopHeader.widgets';

const productDivisions = [
    {
        id: "08eee594-5a55-4b2e-a64f-3ce2e0b5b316",
        name: "Consumer services",
        key: "service"
    },
    {
        id: "55b89e06-78ad-4a35-a932-901e1c036a10",
        name: "Consumer products",
        key: "product"
    }
]

const serviceCategories = [
    {
        id: "08eee594-5a55-4b2e-a64f-3ce2e0b5b316",
        name: "Plumbing"
    },
    {
        id: "55b89e06-78ad-4a35-a932-901e1c036a10",
        name: "Electrician"
    },
    {
        id: "236f44a4-355f-4b51-af06-be2cee221a95",
        name: "Nail Art"
    }
]

const options = {
    title: 'Select Image',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const productDivisonKeys = {
    service: 'service',
    product: 'product'
}


class ShopProductsCreateScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { product, shop } = navigation.state.params;
        return {
            title: ` ${product ? `Update` : `New`} merchandise/service`,
            headerRight: product ? <Button
                buttonStyle={{ marginRight: 10, backgroundColor: 'transparent' }}
                onPress={() => navigation.navigate(`shopProductRemove`, { product, shop })}
                icon={
                    <Icon
                        type='materialcommunityicons'
                        name="delete-forever"
                        size={28}
                        color={Colors.activeTintColor}
                    />
                }
                title={''}
            /> : <Button
                    buttonStyle={{ marginRight: 10, backgroundColor: 'transparent' }}
                    onPress={() => navigation.navigate(`shopDetails`, { shop })}
                    icon={
                        <Icon
                            type='materialcommunityicons'
                            name="cancel"
                            size={28}
                            color={Colors.primary}
                        />
                    }
                    title={''}
                />
        }
    };

    constructor(props) {
        super(props);
        this.shop = props.navigation.state.params.shop;
        this.product = props.navigation.state.params.product;
        this.state = {
            productDivisions: [],
            serviceCategories: [],
            product: {
                productName: '',
                productDivision: '08eee594-5a55-4b2e-a64f-3ce2e0b5b316',
                productDivisonKey: productDivisonKeys.service,
                serviceCategory: '08eee594-5a55-4b2e-a64f-3ce2e0b5b316',
                minStockValue: '',
                sellPrice: '',
                applyDiscount: false,
                discountPercent: '',
                images: [],
            }
        };
    }

    componentDidMount() {
        let { productDivisions: _productDivisions, serviceCategories: _serviceCategories } = this.state;
        _productDivisions = productDivisions.map(productDivision => {
            return {
                label: productDivision.name,
                value: productDivision.id
            }
        })

        _serviceCategories = serviceCategories.map(serviceCategory => {
            return {
                label: serviceCategory.name,
                value: serviceCategory.id
            }
        });


        this.setState({ productDivisions: _productDivisions, serviceCategories: _serviceCategories });

        if (this.product) {


            let { product } = this.state;
            const _product = this.product;

            product.productName = _product.metadata.name;
            product.productDivision = _product.division.id;
            product.productDivisonKey = _product.division.key;
            product.serviceCategory = _product.category.id;
            product.minStockValue = _product.stock.minStock;
            product.sellPrice = _product.price.actualPrice;
            product.applyDiscount = _product.discount.applyDiscount;
            product.discountPercent = _product.discount.discountPercent;
            product.images = _product.multimedia.images;

            this.setState({ product });
        }

    }

    componentDidUpdate(previousProps) {
        if (this.props.shopProduct !== previousProps.shopProduct) {
            if (this.props.shopProduct.success)
                this.props.navigation.navigate('shopDetails');
        }
    }

    handleSelectImage = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            if (response.uri) {
                let { product } = this.state;

                product.images.push({
                    id: (product.images.length + 1).toString(),
                    name: response.fileName,
                    type: response.type,
                    uri: Platform.OS === "android" ? response.uri : response.uri.replace("file://", ""),
                    featured: true
                });

                this.setState({
                    product
                });
            }
        })
    }

    handleInputChange = (key, value) => {
        let { product } = this.state;
        product[key] = value;
        this.setState({ shop: product });
    }

    handleCreateShopProduct = () => {
        const { productDivision, serviceCategory, applyDiscount, discountPercent, images, productName, sellPrice, minStockValue } = this.state.product;
        const division = productDivisions.filter(_productDivision => _productDivision.id === productDivision)[0];
        const shopProduct = {
            id: this.product ? this.product.id : '',
            uid: this.shop.uid,
            metadata: {
                name: productName,
                description: 'Product description'
            },
            price: {
                currency: 'KES',
                actualPrice: sellPrice,
                currentSellPrice: applyDiscount ? (sellPrice * (100 - discountPercent) / 100) : sellPrice
            },
            division,
            shop: {
                id: this.shop.id,
                name: this.shop.metadata.name
            },
            category: serviceCategories.filter(_serviceCategory => _serviceCategory.id === serviceCategory)[0],
            stock: {
                minStock: division.key === productDivisonKeys.service ? 0 : minStockValue,
                stockCount: division.key === productDivisonKeys.service ? 0 : minStockValue
            },
            statistics: {
                quantitySold: 0
            },
            discount: {
                applyDiscount,
                discountPercent: discountPercent,
            }
        }

        let formData = new FormData();

        for (let key in shopProduct) {
            formData.append(key, JSON.stringify(shopProduct[key]));
        }

        images.forEach(image => {
            formData.append(`file`, {
                name: `image_${image.id}`,
                type: image.type,
                uri: image.uri,
                featured: image.featured
            });
        });

        this.props.requestCreateShopProduct({ shopProduct: formData, shopId: this.shop.id, id: this.product ? this.product.id : null });

    }

    handleRemoveImage = (index) => {
        let { product } = this.state;
        product.images.splice(index, 1);
        this.setState({ product });
    }

    renderServiceCategoryPicker = () => {
        let { product } = this.state;
        const productDivision = productDivisions.filter(productDivision => productDivision.id === product.productDivision)[0];
        if (productDivision.key === productDivisonKeys.service) {
            return (
                <View style={{ paddingTop: 15 }}>
                    <Text style={{ fontSize: 22, color: Colors.dark }}>Service Category</Text>
                    <RNPickerSelect
                        value={this.state.product.serviceCategory}
                        onValueChange={(itemValue) => {
                            let { product } = this.state;
                            product.serviceCategory = itemValue;
                            this.setState({ product });
                        }}
                        items={this.state.serviceCategories}
                    />
                </View>
            )
        }
    }

    renderProductNameInputBox = () => {
        let { product } = this.state;
        return (
            <View style={{ paddingTop: 15 }}>
                <Text style={{ fontSize: 22, color: Colors.dark }}>{product.productDivisonKey === productDivisonKeys.service ? `Service` : `Product`} Name</Text>
                <Input
                    containerStyle={{ marginLeft: -10 }}
                    name="productName"
                    value={product.productName}
                    placeholder={`A name for the ${product.productDivisonKey === productDivisonKeys.service ? `service` : `product`}`}
                    onChangeText={text => this.handleInputChange('productName', text)}
                />
            </View>
        )
    }

    renderMinStockValueInputBox = () => {
        let { product } = this.state;
        const category = productDivisions.filter(productDivision => productDivision.id === product.productDivision)[0];
        if (category.key === `product`) {
            return (
                <View style={{ paddingTop: 15 }}>
                    <Text style={{ fontSize: 22, color: Colors.dark }}>Minimum stock</Text>
                    <Input
                        containerStyle={{ marginLeft: -10 }}
                        name="minStockValue"
                        value={this.state.product.minStockValue}
                        placeholder={`Minimum stock value`}
                        onChangeText={text => this.handleInputChange('minStockValue', text)}
                    />
                </View>
            )
        }
    }

    renderSellPriceInputBox = () => {
        return (
            <View style={{ paddingTop: 15 }}>
                <Text style={{ fontSize: 22, color: Colors.dark }}>Starting price</Text>
                <Input
                    containerStyle={{ marginLeft: -10 }}
                    name="sellPrice"
                    value={this.state.product.sellPrice}
                    placeholder={`Starting price`}
                    onChangeText={text => this.handleInputChange('sellPrice', text)}
                />
            </View>
        )
    }

    renderDiscountInputBox = () => {
        const renderInputBox = () => {
            if (this.state.product.applyDiscount) {
                return (
                    <Input
                        containerStyle={{ marginLeft: -10 }}
                        name="discountPercent"
                        value={this.state.product.discountPercent}
                        placeholder={`Discount %`}
                        onChangeText={text => this.handleInputChange('discountPercent', text)}
                    />
                )
            }
        }

        return (
            <View style={{ paddingTop: 15 }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 22, paddingLeft: 5, color: Colors.dark }}>Discount</Text>
                    <Switch
                        value={this.state.product.applyDiscount}
                        onValueChange={(value) => {
                            let { product } = this.state;
                            product.applyDiscount = value;
                            this.setState({ product });
                        }}
                    />
                </View>
                {renderInputBox()}
            </View>
        )
    }

    renderSelectImageButton = () => {
        const { images } = this.state.product;
        if (images.length < 4) {
            return (
                <Button
                    buttonStyle={{ backgroundColor: 'transparent' }}
                    onPress={(e) => this.handleSelectImage()}
                    icon={
                        <Icon
                            type='feather'
                            name="plus-circle"
                            size={28}
                            color="#0275d8"
                        />
                    }
                    title={''}
                />
            )
        }
    }

    renderImageInput = () => {
        return (
            <View style={{ paddingTop: 15 }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 22, paddingLeft: 5, marginTop: 10, color: Colors.dark }}>Gallery images</Text>
                    {this.renderSelectImageButton()}
                </View>
                <View style={{ paddingTop: 15 }}>
                    <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
                        {
                            this.state.product.images.map((image, index) => {
                                return (
                                    <View key={index} style={{ width: 180, height: 180 }}>
                                        <Button
                                            buttonStyle={{ backgroundColor: 'transparent', position: 'absolute', right: 5, top: 0, zIndex: 1 }}
                                            onPress={(e) => this.handleRemoveImage(index)}
                                            icon={
                                                <Icon
                                                    type='antdesign'
                                                    name='closecircleo'
                                                    size={28}
                                                    color={Colors.activeTintColor}
                                                />
                                            }
                                            title={''}
                                        />
                                        <Image
                                            style={{ width: null, height: null, resizeMode: 'contain', flex: 1, marginRight: 10, paddingBottom: 10 }}
                                            source={{ uri: image.uri }}
                                        />
                                    </View>
                                )
                            })
                        }
                    </View>
                </View>
            </View>
        );
    }

    renderAddShopProductButton = () => {
        return (
            <View style={{ paddingTop: 15 }}>
                <Button
                    titleStyle={{
                        fontSize: 18
                    }}
                    title={this.product ? `Update product` : `Create product`}
                    onPress={(e) => this.handleCreateShopProduct()}
                />
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{ marginBottom: 0 }}>

                    <ShopHeaderWidget
                        shop={this.shop}
                        rating={false}
                        deliveriesCount={false}
                        navigation={this.props.navigation}
                    />

                    <View style={{ paddingTop: 15 }}>
                        <Text style={{ fontSize: 22, color: Colors.dark }}>Division</Text>
                        <RNPickerSelect
                            value={this.state.product.productDivision}
                            onValueChange={(itemValue) => {
                                const productDivision = productDivisions.filter(productDivision => productDivision.id === itemValue)[0];
                                let { product } = this.state;
                                product.productDivision = itemValue;
                                product.productDivisonKey = productDivision.key;
                                this.setState({ product });
                            }}
                            items={this.state.productDivisions}
                        />
                    </View>

                    {this.renderServiceCategoryPicker()}
                    {this.renderProductNameInputBox()}
                    {this.renderMinStockValueInputBox()}
                    {this.renderSellPriceInputBox()}
                    {this.renderDiscountInputBox()}
                    {this.renderImageInput()}
                    {this.renderAddShopProductButton()}

                </ScrollView>
            </View >
        )
    }
}

const mapStateToProps = (store) => {
    return {
        shopProduct: store.shopProduct
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestCreateShopProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShopProductsCreateScreen);