import React, { Fragment } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { View, Text, TouchableOpacity, ScrollView, Image } from 'react-native';
import { Button, Icon, Input } from 'react-native-elements';

import styles from '../../../styles/style.default';

import { requestRemoveShopProduct } from '../../../widgets/shop/products/shopProducts.actions';

import Colors from '../../../styles/colors.default';



class ShopProductsRemoveScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: `Remove shop product`
        }
    };

    constructor(props) {
        super(props);
        this.product = props.navigation.state.params.product;
        this.shop = props.navigation.state.params.shop;
    }

    componentDidUpdate(previousProps) {
        if (this.props.shopProduct !== previousProps.shopProduct) {
            if (this.props.shopProduct.success)
                this.props.navigation.navigate('shopDetails');
        }
    }

    handleRemoveShopProduct = () => {
        const product = this.product;
        this.props.requestRemoveShopProduct({ product});
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{ marginBottom: 0 }}>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly', height: 96, marginVertical: -5 }}>
                        <View style={{ width: 96, height: 96, borderRadius: 50, backgroundColor: '#f7f7f7', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={{ width: 88, height: 88, borderRadius: 50 }} onPress={() => this.handleSelectAvatar()}>
                                <Image
                                    style={{ width: null, height: null, resizeMode: 'contain', flex: 1, borderRadius: 50 }}
                                    source={{ uri: this.product.multimedia.images.length ? this.product.multimedia.images[0].uri : 'https://cdn0.iconfinder.com/data/icons/feather/96/no-128.png' }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ paddingTop: 10 }}>
                        <Text style={{ fontSize: 20 }}>This product will be removed and will cease to exist.</Text>
                    </View>

                    <View style={{ paddingTop: 15, flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Button
                            titleStyle={{
                                fontSize: 18
                            }}
                            title={`Cancel`}
                            onPress={() => this.props.navigation.navigate(`shopDetails`, { shop: this.shop })}
                        />
                        <Button
                            titleStyle={{
                                fontSize: 18
                            }}
                            buttonStyle={{
                                backgroundColor: Colors.activeTintColor
                            }}
                            title={`Proceed`}
                            onPress={(e) => this.handleRemoveShopProduct()}
                        />
                    </View>

                </ScrollView>
            </View >
        )
    }
}

const mapStateToProps = (store) => {
    return {
        shopProduct: store.shopProduct
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestRemoveShopProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShopProductsRemoveScreen);