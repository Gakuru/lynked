import React, { Fragment } from 'react';

import { View, Text, TouchableOpacity } from 'react-native';

import styles from '../../../styles/style.default';

const links = [
    {
        title: 'Shops',
        screen: 'shops',
        description: ''
    },
    {
        title: 'Orders',
        screen: 'sellerOrders',
        description: ''
    },
    {
        title: 'Earnings',
        screen: 'earnings',
        description: ''
    }
];

export default class SellerCenterScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Seller center',
        }
    };

    renderSeparator = index => {
        if (index < links.length - 1)
            return (
                <View style={styles.separator} />
            )
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    links.map((link, index) => {
                        return (
                            <Fragment key={index}>
                                <TouchableOpacity
                                    accessibilityRole={'button'}
                                    onPress={() => this.props.navigation.navigate(link.screen)}
                                    style={styles.linkContainer}>
                                    <Text style={styles.link}>{link.title}</Text>
                                    <Text style={styles.description}>{link.description}</Text>
                                </TouchableOpacity>
                                {this.renderSeparator(index)}
                            </Fragment>
                        )
                    })
                }
            </View>
        )
    }
}