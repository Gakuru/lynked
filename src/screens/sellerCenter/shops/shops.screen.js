import React, { Fragment } from 'react';

import { View, Text, StyleSheet, TouchableOpacity, ScrollView, FlatList, Image } from 'react-native';
import Colors from 'react-native/Libraries/NewAppScreen/components/Colors';
import { Button, Icon } from 'react-native-elements';
import ShopsWidget from '../../../widgets/shops/shops.widgets';

import styles from '../../../styles/style.default';

export default class ShopsDetailsScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {

        return {
            title: 'Your shops',
            headerRight: <Button
                buttonStyle={{ marginRight: 10, backgroundColor: 'transparent' }}
                onPress={(e) => navigation.navigate('shopCreate', { shop: null })}
                icon={
                    <Icon
                        type='feather'
                        name="plus-circle"
                        size={28}
                        color="#0275d8"
                    />
                }
                title={''}
            />
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <ShopsWidget navigation={this.props.navigation} />
            </View>
        )
    }
}