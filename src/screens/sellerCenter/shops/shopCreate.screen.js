import React, { Fragment } from 'react';

import { Platform, Dimensions } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { View, Text, StyleSheet, TouchableOpacity, ScrollView, FlatList, Image, Picker } from 'react-native';
import { Button, Icon, Input } from 'react-native-elements';

import ImagePicker from 'react-native-image-picker';

import RNPickerSelect from 'react-native-picker-select';

import styles from '../../../styles/style.default';

import { requestCreateShop } from '../../../widgets/shops/shops.actions';
import Colors from '../../../styles/colors.default';
import { DBSchema } from '../../../schema/database.schema';

// const options = {
//     title: 'Select Avatar',
//     customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
//     storageOptions: {
//         skipBackup: true,
//         path: 'images',
//     },
// };

const shopCategories = [
    {
        id: "73758215-c8e4-4554-a5a2-f8a7bef36b73",
        name: "Merchandise",
    },
    {
        id: "08eee594-5a55-4b2e-a64f-3ce2e0b5b316",
        name: "Consumer services"
    },
    {
        id: "55b89e06-78ad-4a35-a932-901e1c036a10",
        name: "Consumer electronics"
    },
    {
        id: "236f44a4-355f-4b51-af06-be2cee221a95",
        name: "Hair and beauty"
    }
];

class ShopCreateScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { shop } = navigation.state.params;
        return {
            title: shop ? `Update shop` : `New shop`,
            headerRight: shop ? <Button
                buttonStyle={{ marginRight: 10, backgroundColor: 'transparent' }}
                onPress={() => navigation.navigate(`shopRemove`, { shop })}
                icon={
                    <Icon
                        type='materialcommunityicons'
                        name="delete-forever"
                        size={28}
                        color={Colors.activeTintColor}
                    />
                }
                title={''}
            /> : <Button
                    buttonStyle={{ marginRight: 10, backgroundColor: 'transparent' }}
                    onPress={() => navigation.navigate(`shopRemove`, { shop })}
                    icon={
                        <Icon
                            type='materialcommunityicons'
                            name="cancel"
                            size={28}
                            color={Colors.primary}
                        />
                    }
                    title={''}
                />
        }
    };

    constructor(props) {
        super(props);
        this.shop = props.navigation.state.params.shop;
        this.state = {
            user: null,
            categoryPickerItems: [],
            shop: {
                shopName: '',
                shopCategory: '236f44a4-355f-4b51-af06-be2cee221a95',
                description: '',
                shopLogo: {}
            },
            hasFile: false,
        };
    }

    componentDidMount() {
        let { categoryPickerItems } = this.state;
        if (this.shop) {
            let { shop } = this.state;
            shop.id = this.shop.id;
            shop.shopName = this.shop.metadata.name;
            shop.description = this.shop.metadata.description;
            shop.shopCategory = shopCategories.filter(shopCategory => shopCategory.id === this.shop.category.id)[0].id;
            shop.shopLogo = this.shop.multimedia;
            this.setState({ shop });
        }

        categoryPickerItems = shopCategories.map(shopCategory => {
            return {
                label: shopCategory.name,
                value: shopCategory.id
            }
        });

        const user = DBSchema.objects('User').filtered('id != ""')[0];
        if (user)
            this.setState({ user });

    }

    componentDidUpdate(previousProps) {
        if (this.props.shop !== previousProps.shop) {
            if (this.props.shop.success)
                this.props.navigation.navigate('shops');
        }
    }

    handleSelectImage = () => {

        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            if (response.uri) {
                let { shop } = this.state;

                shop.shopLogo = {
                    name: response.fileName,
                    type: response.type,
                    logoUri: Platform.OS === "android" ? response.uri : response.uri.replace("file://", ""),
                    featured: true
                };

                this.setState({
                    shop,
                    hasFile: true
                });

            }
        });
    }

    handleInputChange = (key, value) => {
        let { shop } = this.state;
        shop[key] = value;
        this.setState({ shop });
    }

    handleCreateShop = () => {
        let { shop, user, hasFile } = this.state;
        const category = shopCategories.filter(shopCategory => shopCategory.id === shop.shopCategory)[0];
        const newShop = {
            id: this.shop ? this.shop.id : '',
            uid: user.id,
            metadata: {
                name: shop.shopName,
                description: shop.description
            },
            category
        };

        let formData = new FormData();

        for (let key in newShop) {
            formData.append(key, JSON.stringify(newShop[key]));
        }

        if (hasFile)
            formData.append(`file`, {
                name: `image_${shop.shopLogo.name}`,
                type: shop.shopLogo.type,
                uri: shop.shopLogo.logoUri,
                featured: shop.shopLogo.featured
            });

        this.props.requestCreateShop({ shop: formData, id: this.shop ? this.shop.id : null });
    }

    get categoryPickerItems() {
        return shopCategories.map(shopCategory => {
            return {
                label: shopCategory.name,
                value: shopCategory.id
            }
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{ marginBottom: 0 }}>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly', height: 96 }}>
                        <TouchableOpacity style={{}} onPress={() => this.handleSelectImage()}>
                            <Image
                                style={{ width: 90, height: 90, borderRadius: 50 }}
                                source={{ uri: this.state.shop.shopLogo.logoUri ? this.state.shop.shopLogo.logoUri : 'https://cdn0.iconfinder.com/data/icons/feather/96/no-128.png' }}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{ paddingTop: 10 }}>
                        <Text style={{ fontSize: 22, paddingLeft: 5, color: Colors.dark }}>Name</Text>
                        <Input
                            containerStyle={{ marginLeft: -15 }}
                            leftIconContainerStyle={{ marginLeft: 5 }}
                            leftIcon={{
                                type: 'antdesign',
                                name: 'appstore-o',
                                color: Colors.dark,
                                size: 18
                            }}
                            name="shopName"
                            value={this.state.shop.shopName}
                            placeholder={`A name for your shop`}
                            onChangeText={text => this.handleInputChange('shopName', text)}
                        />
                    </View>

                    <View style={{ paddingTop: 15 }}>
                        <Text style={{ fontSize: 22, paddingLeft: 5, color: Colors.dark }}>Category</Text>
                        <RNPickerSelect
                            onValueChange={(itemValue) => {
                                let { shop } = this.state;
                                shop.shopCategory = itemValue;
                                this.setState({ shop });
                            }}
                            items={this.categoryPickerItems}
                        />
                    </View>

                    <View style={{ paddingTop: 15 }}>
                        <Text style={{ fontSize: 22, fontWeight: '200', paddingLeft: 5, color: Colors.dark }}>About</Text>
                        <Input
                            containerStyle={{ marginLeft: -15 }}
                            leftIconContainerStyle={{ marginLeft: 5 }}
                            leftIcon={{
                                type: 'antdesign',
                                name: 'infocirlceo',
                                color: Colors.dark,
                                size: 18
                            }}
                            name="description"
                            value={this.state.shop.description}
                            placeholder={`Tell the wold about your shop`}
                            onChangeText={text => this.handleInputChange('description', text)}
                        />
                    </View>

                    <View style={{ paddingTop: 15 }}>
                        <Button
                            titleStyle={{
                                fontSize: 18
                            }}
                            title={this.shop ? `Update shop` : `Create shop`}
                            onPress={(e) => this.handleCreateShop()}
                        />
                    </View>

                </ScrollView>
            </View >
        )
    }
}

const mapStateToProps = (store) => {
    return {
        shop: store.shop
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestCreateShop
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShopCreateScreen);