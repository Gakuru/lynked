import React, { Fragment } from 'react';

import { View, ScrollView, Text } from 'react-native';

import styles from '../../../styles/style.default';

import ShopProductsWidgets from '../../../widgets/shop/products/shopProducts.widgets';
import ShopHeaderWidget from '../../../widgets/shop/header/shopHeader.widgets';


export default class ShopsScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { shop } = navigation.state.params;
        return {
            title: `${shop.metadata.name}`
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            shop: this.props.navigation.state.params.shop,
            userResourceControl: this.props.navigation.state.params.userResourceControl
        }
    }

    render() {
        const {shop, userResourceControl} = this.state;
        return (
            <View style={styles.container}>
                <ShopHeaderWidget
                    shop={shop}
                    userResourceControl={userResourceControl}
                    navigation={this.props.navigation}
                />

                <ShopProductsWidgets
                    shop={shop}
                    userResourceControl={userResourceControl}
                    navigation={this.props.navigation}
                />

            </View>
        )
    }
}