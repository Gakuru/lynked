import React from 'react';
import { StyleSheet, View, Text, Image, FlatList, TouchableOpacity } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';

import styles from '../../styles/style.default';

const subCategories = [
    { id: "1", categoryId: "1", name: "Wheat Flour", multimedia: { images: [{ id: "1", uri: "https://cdn.shopify.com/s/files/1/2522/4972/products/whole-wheat-flour-5lb-7151805005-QR.png?v=1543346516" }] } },
    { id: "2", categoryId: "1", name: "Sugar", multimedia: { images: [{ id: "2", uri: "http://angazanews.com/wp-content/uploads/2017/10/42051298b833c1fa9997b06af77d2ca6c82a5061.jpg" }] } },
    { id: "3", categoryId: "1", name: "Salt", multimedia: { images: [{ id: "3", uri: "https://assets.katomcdn.com/q_auto,f_auto/products/409/409-140056/409-140056.jpg" }] } },
    { id: "4", categoryId: "1", name: "Baking Soda", multimedia: { images: [{ id: "4", uri: "https://images-na.ssl-images-amazon.com/images/I/71AaRcTXlHL._SY355_.jpg" }] } },
    { id: "5", categoryId: "2", name: "Soft drinks", multimedia: { images: [{ id: "5", uri: "https://www.laprimacatering.com/images/Product/large/8050.jpg" }] } },
    { id: "6", categoryId: "2", name: "Whisky", multimedia: { images: [{ id: "6", uri: "https://www.cheatsheet.com/wp-content/uploads/2015/02/Alcoholic-Whiskey-Bourbon-in-a-Glass-with-Ice-640x426.jpg" }] } },
    { id: "7", categoryId: "2", name: "Fresh Juices", multimedia: { images: [{ id: "7", uri: "https://previews.123rf.com/images/verca/verca1508/verca150800061/44049737-fresh-juice-with-fruits-on-wooden-table-with-nature-green-background.jpg" }] } },
    { id: "8", categoryId: "2", name: "Energy drinks", multimedia: { images: [{ id: "8", uri: "https://products3.imgix.drizly.com/ci-monster-energy-drink-a44417a571bd8be7.jpeg?auto=format%2Ccompress&dpr=2&fm=jpeg&h=240&q=20" }] } },
    { id: "9", categoryId: "3", name: "Drupes", multimedia: { images: [{ id: "5", uri: "https://nutriliving-images.imgix.net/images/2014/266/1516/41E38DFA-4643-E411-B834-22000AF88B16-250.jpg" }] } },
    { id: "10", categoryId: "3", name: "Berries", multimedia: { images: [{ id: "6", uri: "https://images.agoramedia.com/everydayhealth/gcms/Amazing-Health-Benefits-of-Berries-01-722x406.jpg?width=684" }] } },
    { id: "11", categoryId: "3", name: "Dark-green Leafy vegetables", multimedia: { images: [{ id: "7", uri: "https://youngwomenshealth.org/wp-content/uploads/2012/12/dark-green-vegetables.jpg" }] } },
    { id: "12", categoryId: "4", name: "Plumbing", multimedia: { images: [{ id: "9", uri: "https://cdn.newsapi.com.au/image/v1/7c62c21b674740a243622ed9f639f5ec?width=1024" }] } },
    { id: "13", categoryId: "4", name: "Nail art", multimedia: { images: [{ id: "10", uri: "https://media.istockphoto.com/photos/nail-art-flower-picture-id475371187?k=6&m=475371187&s=612x612&w=0&h=gy5twKBPicVLOrNHxrI5Merr-E6xAw6VRtHKBRH6BO0=" }] } },
    { id: "14", categoryId: "4", name: "Electrician", multimedia: { images: [{ id: "11", uri: "https://www.paulstravelpictures.com/Toyota-Corolla-Headlight-Bulbs-Replacement-Guide/Toyota-Corolla-Headlight-Bulb-Replacement-Guide-005.JPG" }] } },
    { id: "15", categoryId: "4", name: "Carpentry", multimedia: { images: [{ id: "12", uri: "https://4.imimg.com/data4/PT/KU/MY-26645838/wooden-carpentry-work-500x500.jpg" }] } },
    { id: "16", categoryId: "4", name: "Cleaning & Gardening", multimedia: { images: [{ id: "13", uri: "http://www.workbusters.co.uk/wp-content/uploads/2015/03/gardening.jpg" }] } },
]

export default class CategoryItemsScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.state.params.category.name,
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            subCategories: subCategories.filter(subCategory => subCategory.categoryId === this.props.navigation.state.params.category.id)
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={this.state.subCategories}
                        keyExtractor={subCategory => subCategory.id}
                        renderItem={({ item: subCategory }) => {
                            return (
                                <TouchableOpacity
                                    style={{ flex: 1, flexDirection: 'row', marginBottom: 5, height: 80, alignItems: 'center' }}
                                    onPress={(e) => this.props.navigation.navigate('products', { subCategory })}>
                                    <View style={{}}>
                                        <Image
                                            style={{ width: 72, height: 72, borderRadius: 5 }}
                                            source={{ uri: subCategory.multimedia.images[0].uri }}
                                        />
                                    </View>
                                    <View style={{ padding: 10 }}>
                                        <Text style={{ fontSize: 16 }}>{subCategory.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }}
                    />
                </View>
            </SafeAreaView>
        )
    }
}