import React, { useState } from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, StyleSheet, SafeAreaView } from 'react-native';

import { Button } from 'react-native-elements';
import SearchWidget from '../../widgets/search/Search.widget';

import styles from '../../styles/style.default';

const categories = [
    { id: '1', name: 'FoodStuffs', multimedia: { image: { uri: 'https://previews.123rf.com/images/alexraths/alexraths1707/alexraths170700003/81386931-fresh-vegetables-and-fruits-background.jpg' } } },
    { id: '2', name: 'Bevergages', multimedia: { image: { uri: 'https://www.dollargeneral.com/media/catalog/category/DG_436_89348_eComm_CategoryAssets_Beverage_400x400.jpg' } } },
    { id: '3', name: 'Fruits & Vegetables', multimedia: { image: { uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvAl81yNbu24P_4nU5tn1oJwR8fctrYHNwBh-trNAtrqV8CQuk' } } },
    { id: '4', name: 'Services', multimedia: { image: { uri: 'https://zone-thebestsingapore-bhxtb9xxzrrdpzhqr.netdna-ssl.com/wp-content/uploads/2018/05/Plumbing-Services.jpg' } } },
    { id: '5', name: 'Hair & Beauty', multimedia: { image: { uri: 'https://www.tocklocks.co.uk/_webedit/cached-images/69-0-0-1017-10000-5140-1152.jpg' } } },
    { id: '6', name: 'Home & Gardening', multimedia: { image: { uri: 'https://www.rhs.org.uk/getmedia/d5f84a19-3211-416e-b0f0-399254629563/watering-garden-1200x675.jpg' } } },
    { id: '8', name: 'Consumer Electronics', multimedia: { image: { uri: 'https://banner2.kisspng.com/20180619/rzr/kisspng-consumer-electronics-huawei-p8-lite-2017-home-th-nad-electronics-5b2979e9ed8538.4313172215294448419729.jpg' } } },
];

export default class CategoriesScreen extends React.Component {

    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            showSearchBar: false,
            categories: [categories]
        }
    }

    renderMenu = () => {
        if (this.state.showSearchBar) {
            return (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View>
                        <Button
                            icon={{
                                type: 'Feather',
                                name: 'arrow-back',
                                color: 'black',
                            }}
                            buttonStyle={{
                                borderWidth: 0,
                                backgroundColor: "transparent",
                            }}
                            textStyle={{
                                fontWeight: 'bold'
                            }}
                            onPress={() => this.setState({ showSearchBar: false })}
                        />
                    </View>
                    <View style={{ width: '80%' }}>
                        <SearchWidget />
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 10, alignItems: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start' }}>
                        <Text style={{ color: 'black', fontSize: 22, fontWeight: 'bold' }}>Categories</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <Button
                            icon={{
                                type: 'Feather',
                                name: 'search',
                                color: 'black',
                                size: 20,
                            }}
                            buttonStyle={{
                                borderWidth: 0,
                                backgroundColor: "transparent",
                            }}
                            textStyle={{
                                fontWeight: 'bold'
                            }}
                            onPress={() => this.setState({ showSearchBar: true })}
                        />
                        <Button
                            title='...'
                            buttonStyle={{
                                borderWidth: 0,
                                backgroundColor: "white",
                                marginTop: -2
                            }}
                            titleStyle={{
                                fontWeight: 'bold',
                                color: 'black',
                                fontSize: 20,
                                transform: [{ rotate: '90deg' }]
                            }}
                            onPress={() => console.log('more icon')}
                        />
                    </View>
                </View>
            )
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ backgroundColor: 'white', width: '100%', height: 60, justifyContent: 'center', shadowOpacity: 0.5, shadowRadius: 5, shadowColor: 'black', elevation: 4, shadowOffset: { width: 10, height: 10, } }}>
                    {this.renderMenu()}
                </View>
                <View style={{ flex: 1, padding: 10 }}>
                    <FlatList
                        data={categories}
                        keyExtractor={category => category.id}
                        renderItem={({ item: category }) => {
                            return (
                                <TouchableOpacity
                                    style={{ flex: 1, flexDirection: 'row', marginBottom: 5, height: 80, alignItems: 'center' }}
                                    onPress={(e) => this.props.navigation.navigate('categoryItems', { category })}>
                                    <View style={{}}>
                                        <Image
                                            style={{ width: 64, height: 64, borderRadius: 5 }}
                                            source={{ uri: category.multimedia.image.uri }}
                                        />
                                    </View>
                                    <View style={{ padding: 10 }}>
                                        <Text style={{ fontSize: 16 }}>{category.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        }}
                    />
                </View>
            </SafeAreaView >
        );
    }
}

// const styles = StyleSheet.create({
//     container: {
//         backgroundColor: '#f2f2f2',
//     }
// });