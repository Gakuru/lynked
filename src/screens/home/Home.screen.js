import React, { Component } from 'react';
import { StyleSheet, Text, SafeAreaView, View, ScrollView, Image, FlatList } from 'react-native';

import SearchWidget from '../../widgets/search/Search.widget';
import CarouselWidget from '../../widgets/carousel/Carousel.widget';
import LandingPageCategoriesWidget from '../../widgets/landingPageCategories/LandingPageCategories.widget';
import PopularCategoriesWidget from '../../widgets/popularCategories/PopularCategories.widget';
import LandingPageProductsWidget from '../../widgets/landingPageProducts/LandingPageProducts.widget';

export default class HomeScreen extends Component {

    static navigationOptions = {
        header: null,
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <SearchWidget />
                <CarouselWidget />
                <PopularCategoriesWidget />
                {/* <LandingPageCategoriesWidget /> */}
                <LandingPageProductsWidget
                    navigation={this.props.navigation}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f2f2f2',
        flex: 1,
    }
});
