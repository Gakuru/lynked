import React, { Fragment } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';

import styles from '../../styles/style.default';
import { DBSchema } from '../../schema/database.schema';

const links = [
    {
        title: 'Profile',
        screen: 'profile',
        description: ''
    },
    {
        title: 'Orders',
        screen: 'orders',
        description: ''
    },
    {
        title: 'Seller center',
        screen: 'sellerCenter',
        description: ''
    },
    {
        title: 'Logout',
        screen: '',
        description: ''
    }
];

export default class AccountScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Account',
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            user: null
        };
    }

    componentDidMount() {
        let { user } = this.state;
        user = DBSchema.objects('User');
        if (user) {
            this.setState({ user })
        }
    }

    renderSeparator = index => {
        if (index < links.length - 1)
            return (
                <View style={styles.separator} />
            )
    }

    handleLogOut = () => {
        DBSchema.write(() => {
            DBSchema.delete(this.state.user);
            this.setState({ user: null });
            this.props.navigation.navigate('Auth');
        });
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    links.map((link, index) => {
                        return (
                            <Fragment key={index}>
                                <TouchableOpacity
                                    accessibilityRole={'button'}
                                    onPress={() => link.screen ? this.props.navigation.navigate(link.screen) : this.handleLogOut()}
                                    style={styles.linkContainer}>
                                    <Text style={styles.link}>{link.title}</Text>
                                    <Text style={styles.description}>{link.description}</Text>
                                </TouchableOpacity>
                                {this.renderSeparator(index)}
                            </Fragment>
                        )
                    })
                }
            </View>
        )
    }
}