import { REQUEST_SERVICE_SUCCESS } from "./product.types"

export const productOverview = (state = {}, { type, request }) => {
    switch (type) {
        case REQUEST_SERVICE_SUCCESS:
            {
                return request;
            }
        default:
            {
                return state;
            }
    }
}