import { REQUEST_REQUEST_SERVICE, REQUEST_SERVICE_SUCCESS } from "./product.types";

export const requestService = request => ({ type: REQUEST_REQUEST_SERVICE, request });
export const requestServiceSuccess = request => ({ type: REQUEST_SERVICE_SUCCESS, request });