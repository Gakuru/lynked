import React from 'react';
import { View, Text } from 'react-native';

import styles from '../../styles/style.default'

export default class ShopProductsOverviewScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: `Store products`
        }
    }

    constructor(props){
        super(props);
        this.state = {
            shopProducts: props.shopProducts
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>List of products sold by the particular shop</Text>
            </View>
        )
    }
}