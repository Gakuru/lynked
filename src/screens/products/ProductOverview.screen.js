import React from 'react';
import { View, Text, Image, ActivityIndicator } from 'react-native';

import Carousel from 'react-native-snap-carousel';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from '../../styles/style.default'
import { Button } from 'react-native-elements';
import colors from '../../styles/colors.default';

import { DBSchema } from '../../schema/database.schema';

import { requestService } from './product.actions';

class ProductsScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: `Product Overview`
        }
    };

    constructor(props) {
        super(props);
        this.product = this.props.navigation.state.params.product;

        this.state = {
            sliderWidth: 200,
            itemWidth: 100,
            user: null,
        }
    }

    componentDidMount() {
        const user = DBSchema.objects('User').filtered('id != ""')[0];
        if (user)
            this.setState({ user });
    }

    renderCarouselItem = ({ item, index }) => {
        return (
            <Image
                style={{ width: '100%', height: 160, borderRadius: 7, alignSelf: 'stretch' }}
                source={{ uri: item.uri }}
            />
        );
    }

    renderCarousel = () => {
        if (this.product.multimedia.images) {
            return (
                <View style={{}} onLayout={(event) => {
                    const { width } = event.nativeEvent.layout;
                    this.setState({ sliderWidth: width, itemWidth: width });
                }}>
                    <Carousel
                        autoplay={true}
                        loop={true}
                        data={this.product.multimedia.images}
                        renderItem={this.renderCarouselItem}
                        sliderWidth={this.state.itemWidth}
                        itemWidth={this.state.itemWidth}
                    />
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='small' color='#0000ff' />
                </View>
            );
        }
    }

    renderServicesButtonsToolBar = () => {
        return (
            <View style={{ paddingTop: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{}}>
                    <Button
                        icon={{
                            type: 'MaterialIcons',
                            name: 'store',
                            color: colors.activeTintColor,
                        }}
                        buttonStyle={{
                            backgroundColor: colors.white
                        }}
                        titleStyle={{
                            color: colors.black,
                            fontWeight: 'bold'
                        }}
                        onPress={() => this.props.navigation.navigate('ShopProductsOverview')}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <Button
                        title={`REQUEST NOW`}
                        buttonStyle={{
                            backgroundColor: colors.fadedTintColor
                        }}
                        titleStyle={{
                            color: colors.white,
                        }}
                        onPress={() => {
                            const { user } = this.state;
                            console.log(user);
                            const serviceRequest = {
                                requester_id: user.id,
                                provider_id: this.product.uid,
                                service_id: this.product.id,
                                device_token: user.deviceToken,
                            }
                            this.props.requestService(serviceRequest);
                        }}
                    />
                </View>
            </View>
        )
    }

    renderMerchandiseButtonsToolBar = () => {
        return (
            <View style={{ paddingTop: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{}}>
                    <Button
                        icon={{
                            type: 'MaterialIcons',
                            name: 'store',
                            color: colors.activeTintColor,
                        }}
                        buttonStyle={{
                            backgroundColor: colors.white
                        }}
                        titleStyle={{
                            color: colors.black,
                            fontWeight: 'bold'
                        }}
                        onPress={() => this.props.navigation.navigate('ShopProductsOverview')}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <Button
                        title={`ADD TO CART`}
                        buttonStyle={{
                            backgroundColor: colors.lightTintColor
                        }}
                        titleStyle={{
                            color: colors.fadedTintColor,
                        }}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <Button
                        title={`BUY NOW`}
                        buttonStyle={{
                            backgroundColor: colors.fadedTintColor
                        }}
                        titleStyle={{
                            color: colors.white,
                        }}
                    />
                </View>
            </View>
        )
    }

    renderButtonsToolBar = () => {
        if (this.product.division.key === `service`) {
            return this.renderServicesButtonsToolBar();
        } else {
            return this.renderMerchandiseButtonsToolBar();
        }
    }

    renderItemPrice = () => {
        const { price, discount } = this.product;
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={{ fontSize: 24, fontWeight: 'bold' }}>{`${price.currency} ${price.currentSellPrice}`}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ color: colors.white }}>
                        <Text style={{ textDecorationLine: 'line-through', color: colors.fadedGray }}>{`${price.currency} ${price.actualPrice}`}</Text>
                    </View>
                    <View style={{ backgroundColor: colors.lightTintColor, marginLeft: 5 }}>
                        <Text style={{ color: colors.activeTintColor }}>{discount.applyDiscount ? `${(discount.discountPercent * -1)}%` : ``}</Text>
                    </View>
                </View>
            </View>
        )
    }

    renderProductDescription = () => {
        const { division, category, metadata } = this.product;
        return (
            <View style={{ marginTop: 10 }}>
                <Text style={{ fontSize: 22, color: colors.darkGray }}>{`${metadata.name}`}</Text>
                <Text style={{ fontSize: 12, color: colors.black }}>{`${category.name}`}</Text>
                <Text style={{ fontSize: 12, color: colors.black }}>{`${division.name}`}</Text>
            </View>
        )
    }

    get daysToShip() {
        return (
            <Text style={{ color: colors.fadedGray, fontSize: 16 }}>
                <Text>
                    To 'your current shipping location' it will take approximately
                </Text>
                <Text style={{ fontWeight: 'bold', color: colors.black }}>
                    &nbsp;{`1 - 2`}
                </Text>
                <Text>
                    &nbsp;{`days`}
                </Text>
            </Text>
        )
    }

    renderShippingDetails = () => {
        return (
            <View style={{ marginTop: 10, borderTopWidth: 1, borderTopColor: colors.fadedGray }}>
                <View style={{ paddingTop: 5 }}>
                    <Text style={{ fontSize: 24, fontWeight: 'bold' }}>{`Shipping: KES 70.00`}</Text>
                </View>
                <View style={{ paddingTop: 5 }}>
                    <View>
                        {this.daysToShip}
                    </View>
                </View>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderCarousel()}
                {this.renderButtonsToolBar()}
                {this.renderItemPrice()}
                {this.renderProductDescription()}
                {this.renderShippingDetails()}
            </View>
        )
    }
}


const mapStateToProps = (store) => {
    return {
        productOverview: store.productOverview
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestService
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductsScreen);