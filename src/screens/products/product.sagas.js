import { put, call, takeEvery, all } from 'redux-saga/effects';
import { REQUEST_REQUEST_SERVICE } from './product.types';
import { requestServiceSuccess } from './product.actions';
import { baseURL } from '../../api/url';

const _requestService = async ({ request }) => {
    try {
        const response = await fetch(`${baseURL}requests`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': '',
            },
            body: JSON.stringify(request)
        });
        return await response ? response.json() : null;
    } catch (e) {
        console.log(e);
    }
}

function* requestService(params) {
    try {
        const data = yield call(_requestService, params);
        yield put(requestServiceSuccess(data.request));
    } catch (err) {
        console.log(err);
    }
}

export default function* productOverviewSaga() {
    yield all([
        takeEvery(REQUEST_REQUEST_SERVICE, requestService),
    ]);
}