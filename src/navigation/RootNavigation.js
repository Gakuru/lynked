import React from 'react';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Feather from 'react-native-vector-icons/Feather';

import HomeScreen from '../screens/home/Home.screen';
import CategoriesScreen from '../screens/categories/Categories.screen';
import SubCategoriesScreen from '../screens/subcategories/Subcategories.screen';
import ProductsScreen from '../screens/products/ProductOverview.screen';
import AccountScreen from '../screens/account/Account.screen';
import SellerCenterScreen from '../screens/sellerCenter/center/SellerCenter.screen';

import { Text, View } from 'react-native';
import ProfileScreen from '../screens/profile/Profile.screen';
import OrdersScreen from '../screens/orders/Orders.screen';

import SellerOrdersScreen from '../screens/sellerCenter/orders/sellerOrders.screen';
import ShopsScreen from '../screens/sellerCenter/shops/shops.screen';
import ShopsDetailsScreen from '../screens/sellerCenter/shops/shopDetails.screen';
import ShopCreateScreen from '../screens/sellerCenter/shops/shopCreate.screen';
import ShopRemoveScreen from '../screens/sellerCenter/shops/shopRemove.screen';
import ShopProductsOverviewScreen from '../screens/products/shopProductsOverview.screen';


import shopProductsCreateScreen from '../screens/sellerCenter/products/shopProductsCreate.screen';
import shopProductsRemoveScreen from '../screens/sellerCenter/products/shopProductsRemove.screen';

import LoginScreen from '../screens/auth/Login.screen';

import Colors from '../styles/colors.default';

import { DBSchema } from '../schema/database.schema';

const user = DBSchema.objects('User').length;

const HomeStack = createStackNavigator({
    home: {
        screen: HomeScreen,
    },
    productOverview: {
        screen: ProductsScreen,
    },
    ShopProductsOverview:{
        screen: ShopProductsOverviewScreen
    }
})

const CategoryStack = createStackNavigator({
    category: {
        screen: CategoriesScreen
    },
    categoryItems: {
        screen: SubCategoriesScreen
    }
})

const CartStack = createStackNavigator({
    cart: {
        screen: () => {
            return (
                <View>
                    <Text>Shopping cart screen</Text>
                </View>
            )
        }
    },
})

const AccountStack = createStackNavigator({
    account: {
        screen: AccountScreen
    },
    profile: {
        screen: ProfileScreen
    },
    orders: {
        screen: OrdersScreen
    },
    sellerCenter: {
        screen: SellerCenterScreen
    },
    sellerOrders: {
        screen: SellerOrdersScreen
    },
    shops: {
        screen: ShopsScreen
    },
    shopDetails: {
        screen: ShopsDetailsScreen
    },
    shopCreate: {
        screen: ShopCreateScreen
    },
    shopRemove: {
        screen: ShopRemoveScreen
    },
    shopProductCreate: {
        screen: shopProductsCreateScreen
    },
    shopProductRemove: {
        screen: shopProductsRemoveScreen
    }
});

const BottomAppTabNavigator = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <Feather name="home" size={24} color={tintColor} />
                )
            },
        },
        Category: {
            screen: CategoryStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <Feather name="list" size={24} color={tintColor} />
                )
            },
        },
        Cart: {
            screen: CartStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <Feather name="shopping-cart" size={24} color={tintColor} />
                )
            },
        },
        Account: {
            screen: AccountStack,
            navigationOptions: {
                tabBarIcon: ({ tintColor, focused }) => (
                    <Feather name="user" size={24} color={tintColor} />
                )
            },
        },
    },
    {
        tabBarOptions: {
            showLabel: true,
            activeTintColor: Colors.activeTintColor,
            inactiveTintColor: Colors.inactiveTintColor,
        }
    }
);

export default createAppContainer(createSwitchNavigator({
    App: BottomAppTabNavigator,
    Auth: LoginScreen,
},
    {
        initialRouteName: user ? `App` : `Auth`,
    }
));