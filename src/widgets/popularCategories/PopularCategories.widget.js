import React, { useState, useEffect } from 'react';

import { View, Text, FlatList, Image } from 'react-native';

const PopularCategoriesWidget = () => {

    const [popularCategories, setPopularCategories] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`http://localhost:3000/popularCategories`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'x-api-key': ''
                    }
                });
                setPopularCategories(await response.json());
            } catch (e) {
                console.log(e);
            }
        }
        fetchData()
    }, [])

    return (
        <View style={{ padding: 5, height: 100 }}>
            {renderPopularCategories(popularCategories)}
        </View>
    )
}

const renderPopularCategories = (popularCategories) => {
    if (popularCategories) {
        return (
            <FlatList
                horizontal={true}
                data={popularCategories}
                keyExtractor={item => item.id}
                renderItem={renderPopularCategoryItem}
            />
        )
    }
}

const renderPopularCategoryItem = ({ item, index }) => {
    return (
        <View style={{ margin: 10, flex: 1, justifyContent: 'space-evenly', width: 80 }}>
            <Image
                style={{ width: 40, height: 40, borderRadius: 50, marginLeft: 20 }}
                source={{ uri: item.uri }}
            />
            <Text style={{ fontSize: 12, textAlign: 'center' }} textBreakStrategy={'simple'} numberOfLines={2}>{item.name}</Text>
        </View>
    );
}

export default PopularCategoriesWidget;

// import { bindActionCreators } from 'redux';
// import { connect } from 'react-redux';


// import { requestGetPopularCategories } from './popularCategories.actions';

// class PopularCategoriesWidget extends React.Component {

//     componentDidMount() {
//         this.props.requestGetPopularCategories();
//     }

//     renderPopularCategoryItem = ({ item, index }) => {
//         return (
//             <View style={{ margin: 10, flex: 1, justifyContent: 'space-evenly', width: 80 }}>
//                 <Image
//                     style={{ width: 40, height: 40, borderRadius: 50, marginLeft:20 }}
//                     source={{ uri: item.uri }}
//                 />
//                 <Text style={{ fontSize: 12, textAlign: 'center' }} textBreakStrategy={'simple'} numberOfLines={2}>{item.name}</Text>
//             </View>
//         );
//     }

//     renderPopularCategories = () => {
//         if (this.props.popularCategories) {
//             return (
//                 <FlatList
//                     horizontal={true}
//                     data={this.props.popularCategories}
//                     keyExtractor={item => item.id}
//                     renderItem={this.renderPopularCategoryItem}
//                 />
//             )
//         }
//     }

//     render() {
//         {/* @Todo extract featured products component */ }
//         return (
//             <View style={{ padding: 5, height: 100 }}>
//                 {this.renderPopularCategories()}
//             </View>
//         )
//         {/* End featured products component */ }
//     }
// }

// const mapStateToProps = (store) => {
//     return {
//         popularCategories: store.popularCategories
//     }
// }

// const mapDispatchToProps = (dispatch) => bindActionCreators({
//     requestGetPopularCategories
// }, dispatch);

// export default connect(mapStateToProps, mapDispatchToProps)(PopularCategoriesWidget);