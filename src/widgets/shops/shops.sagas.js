import { put, call, takeEvery, all } from 'redux-saga/effects';
import { REQUEST_GET_SHOPS, REQUEST_CREATE_SHOP, REQUEST_REMOVE_SHOP } from './shops.types';
import { getShopsSuccess, CreateShopSuccess, removeShopSuccess } from './shops.actions';
import { baseURL } from '../../api/url';

const __getShops = async ({ userId }) => {
    try {
        const response = await fetch(`${baseURL}shops?uid=${userId}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

const __createShop = async ({ shop, id }) => {
    try {
        const response = await fetch(`${baseURL}shops`, {
            method: id ? `PATCH` : `POST`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            },
            body: shop
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

const __removeShop = async ({ shop }) => {
    try {
        const response = await fetch(`${baseURL}shops`, {
            method: `DELETE`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            },
            body: JSON.stringify(shop)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

function* getShops({ userId }) {
    try {
        const data = yield call(__getShops, userId);
        yield put(getShopsSuccess(data.shops));
    } catch (err) {
        console.log(err);
    }
}

function* createShop({ params }) {
    try {
        const data = yield call(__createShop, params);
        yield put(CreateShopSuccess(data));
    } catch (err) {
        console.log(err);
    }
}

function* removeShop({ params }) {
    try {
        const data = yield call(__removeShop, params);
        yield put(removeShopSuccess(data));
    } catch (err) {
        console.log(err);
    }
}

export default function* shopsSaga() {
    yield all([
        takeEvery(REQUEST_GET_SHOPS, getShops),
        takeEvery(REQUEST_CREATE_SHOP, createShop),
        takeEvery(REQUEST_REMOVE_SHOP, removeShop),
    ]);
}