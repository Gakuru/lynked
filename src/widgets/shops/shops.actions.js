import {
    REQUEST_GET_SHOPS,
    GET_SHOPS_SUCCESS,

    REQUEST_CREATE_SHOP,
    CREATE_SHOP_SUCCESS,

    REQUEST_REMOVE_SHOP,
    REMOVE_SHOP_SUCCESS
} from "./shops.types";

export const requestGetShops = (userId) => ({ type: REQUEST_GET_SHOPS, userId });
export const getShopsSuccess = (shops) => ({ type: GET_SHOPS_SUCCESS, shops });

export const requestCreateShop = (params) => ({ type: REQUEST_CREATE_SHOP, params });
export const CreateShopSuccess = (shop) => ({ type: CREATE_SHOP_SUCCESS, shop });

export const requestRemoveShop = (params) => ({ type: REQUEST_REMOVE_SHOP, params });
export const removeShopSuccess = (shop) => ({ type: REMOVE_SHOP_SUCCESS, shop });