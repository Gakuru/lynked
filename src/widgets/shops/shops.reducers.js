import {
    GET_SHOPS_SUCCESS,
    CREATE_SHOP_SUCCESS,
    REMOVE_SHOP_SUCCESS
} from "./shops.types";

export const shops = (state = null, { type, shops }) => {
    switch (type) {
        case GET_SHOPS_SUCCESS:
            {
                return shops
            }
        default:
            {
                return state
            }
    }
}

export const shop = (state = null, { type, shop }) => {
    switch (type) {
        case REMOVE_SHOP_SUCCESS:
            {
                return shop
            }
        case CREATE_SHOP_SUCCESS:
            {
                return shop
            }
        default:
            {
                return state
            }
    }
}