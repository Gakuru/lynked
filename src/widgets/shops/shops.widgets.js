import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { View, Image, Text, StyleSheet, FlatList, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';

import { requestGetShops } from './shops.actions';

import styles from '../../styles/style.default';
import { DBSchema } from '../../schema/database.schema';

class ShopsWidget extends React.Component {

    constructor(props) {
        super(props);
        this.user = null;
        this.state = {
            user: null,
            shop: props.navigation.state.shop,
            loading: false
        }

        this.props.navigation.addListener('willFocus', () => {
            this.getShopsList();
        })

    }

    componentDidMount() {
        this.user = DBSchema.objects('User').filtered('id != ""')[0];
        if (this.user)
            this.getShopsList()
    }

    getShopsList = () => {
        this.props.requestGetShops({ userId: this.user.id });
    }

    renderSeparator = index => {
        if (index < this.props.shops.length - 1)
            return (
                <View style={styles.separator} />
            )
    }

    renderShopItem = () => {
        return (
            <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
                <Text>Rendor shop products item</Text>
            </View>
        );
    }

    renderShops = () => {
        if (this.props.shops) {
            return (
                <FlatList
                    data={this.props.shops}
                    keyExtractor={shop => shop.id}
                    onRefresh={() => this.getShopsList()}
                    refreshing={this.state.loading}
                    renderItem={({ item: shop }) => {
                        return (
                            <View
                                style={{ flex: 1, flexDirection: 'row', marginBottom: 5, height: 96, alignItems: 'center', backgroundColor: 'white', borderRadius: 5 }}>

                                <TouchableOpacity style={{ paddingLeft: 10 }}
                                    onPress={e => this.props.navigation.navigate('shopCreate', { shop: shop })}>
                                    <Image
                                        style={{ width: 90, height: 90, borderRadius: 50 }}
                                        source={{ uri: shop.multimedia.logoUri ? shop.multimedia.logoUri : 'https://cdn0.iconfinder.com/data/icons/feather/96/no-128.png' }}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{ padding: 10 }}
                                    onPress={e => this.props.navigation.navigate('shopDetails', { shop: shop })}>
                                    <View style={{ paddingLeft: 5 }}>
                                        <Text style={{ fontSize: 16, fontFamily: 'System', fontWeight: 'bold' }}>{shop.metadata.name}</Text>
                                    </View>
                                    <View style={{ padding: 5 }}>
                                        <Text style={{ fontSize: 13, fontFamily: 'System', fontWeight: 'bold' }}>{shop.category.name}</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <View style={{ padding: 5 }}>
                                            <Text style={{ fontSize: 11, fontFamily: 'System' }}>{shop.analysis.deliveriesCount}+ deliveries</Text>
                                        </View>
                                        <View style={{ padding: 5 }}>
                                            <Text style={{ fontSize: 11, fontFamily: 'System' }}>{shop.analysis.starRatingAverage} star rating</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        );
                    }}
                />
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderShops()}
            </View>
        )
    }

}

const mapStateToProps = (store) => {
    return {
        shops: store.shops
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetShops
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShopsWidget);