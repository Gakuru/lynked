import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { View, Text, Image, FlatList } from 'react-native';

import { requestGetLandingPageCategories } from './LandingPageCategories.actions';

class LandingPageCategoriesWidget extends React.Component {

    componentDidMount() {
        this.props.requestGetLandingPageCategories();
    }

    renderLandingPageCategoryItem = ({ item, index }) => {
        return (
            <View style={{ backgroundColor: '#ffffff', width: 84, height: 84, borderRadius: 5, marginRight: 10 }}>
                <Image
                    style={{ width: null, height: 64, borderTopRightRadius: 5, borderTopLeftRadius: 5 }}
                    source={{ uri: item.uri }}
                />
            </View>
        );
    }

    renderLandingPageCategories = () => {
        if (this.props.landingPageCategories) {
            return (
                <FlatList
                    data={this.props.landingPageCategories}
                    keyExtractor={item => item.id}
                    renderItem={({ item }) => {
                        return (
                            <View style={{ height: 140, padding: 10, marginBottom: 20, backgroundColor: item.color, borderRadius: 7, shadowOpacity: 0.5, shadowRadius: 5, shadowColor: 'black', elevation: 4, shadowOffset: { width: 10, height: 10 } }}>
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ zIndex: 1 }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'black' }}>{item.name}</Text>
                                        <View style={{ position: 'absolute', bottom: 0 }}>
                                            <FlatList data={item.featuredProducts}
                                                keyExtractor={item => item.id}
                                                renderItem={this.renderLandingPageCategoryItem}
                                                numColumns={3}
                                            />
                                        </View>
                                    </View>
                                    <View>
                                        <Image
                                            style={{ width: 120, height: 140, margin: -10 }}
                                            source={{ uri: item.uri }}
                                        />
                                    </View>
                                </View>
                            </View>
                        );
                    }}
                />
            )
        }
    }

    render() {
        {/* @Todo extract landing page categories */ }
        return (
            <View style={{ padding: 10 }}>
                {this.renderLandingPageCategories()}
            </View>
        )
        {/* End landing page categories */ }
    }
}

const mapStateToProps = (store) => {
    return {
        landingPageCategories: store.landingPageCategories
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetLandingPageCategories
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LandingPageCategoriesWidget);