import { put, call, takeEvery, all } from 'redux-saga/effects';
import { REQUEST_GET_LANDING_PAGE_CATEGORIES } from './LandingPageCategories.types';
import { getLandingPageCategoriesSuccess } from './LandingPageCategories.actions';

const __getLandingPageCategories = async () => {
    try {
        const response = await fetch(`http://localhost:3000/landingPageCategories `, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

function* getLandingPageCategories() {
    try {
        const data = yield call(__getLandingPageCategories);
        yield put(getLandingPageCategoriesSuccess(data));
    } catch (err) {
        console.log(err);
    }
}

export default function* landingPageCategoriesSaga() {
    yield all([
        takeEvery(REQUEST_GET_LANDING_PAGE_CATEGORIES, getLandingPageCategories),
    ]);
}