import { GET_LANDING_PAGE_CATEGORIES_SUCCESS } from "./LandingPageCategories.types";

export const landingPageCategories = (state = null, { type, landingPageCategories }) => {
    switch (type) {
        case GET_LANDING_PAGE_CATEGORIES_SUCCESS:
            {
                return landingPageCategories
            }
        default:
            {
                return state
            }
    }
}