import {
    GET_LANDING_PAGE_CATEGORIES_SUCCESS,
    REQUEST_GET_LANDING_PAGE_CATEGORIES
} from "./LandingPageCategories.types";

export const requestGetLandingPageCategories = () => ({ type: REQUEST_GET_LANDING_PAGE_CATEGORIES })
export const getLandingPageCategoriesSuccess = landingPageCategories => ({ type: GET_LANDING_PAGE_CATEGORIES_SUCCESS, landingPageCategories })