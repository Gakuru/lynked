import React from 'react';
import {View} from 'react-native';
import { SearchBar } from 'react-native-elements';
import Colors from '../../styles/colors.default';

const searchPlaceholders = [
    { id: '2', caption: 'Search pros...' },
    { id: '3', caption: 'Search car parts...' },
    { id: '3', caption: 'Search makeup experts...' },
];

export default class SearchWidget extends React.Component {

    state = {
        searchText: '',
        searchPlaceholderText: 'Search here...'
    };

    componentDidMount() {
        this.randomPlaceholderText;
    }

    get randomPlaceholderText() {
        window.setInterval(() => {
            const randomPlaceholderIndex = Math.floor(Math.random() * searchPlaceholders.length);
            const searchPlaceholder = searchPlaceholders[randomPlaceholderIndex];
            const searchPlaceholderText = searchPlaceholder.caption;
            this.setState({ searchPlaceholderText });
        }, 5000);
    }

    handleSearch = searchText => {
        this.setState({ searchText });
    };

    render() {
        {/* @Todo extract search component */ }
        const { searchText } = this.state;
        return (
            <View style={{ backgroundColor: 'white', width: '100%', paddingLeft: 10, paddingRight: 10, height: 60, justifyContent: 'center', shadowOpacity: 0.5, shadowRadius: 5, shadowColor: 'black', elevation: 4, shadowOffset: { width: 10, height: 10, } }}>
                <SearchBar
                    containerStyle={{ backgroundColor: Colors.faded, height: 45, borderColor: 'transparent', borderTopColor: 'transparent', borderBottomColor: 'transparent', borderWidth: 1, borderRadius: 30 }}
                    inputContainerStyle={{ backgroundColor: Colors.faded, bottom: 6, borderRadius: 30, height: 40 }}
                    placeholder={this.state.searchPlaceholderText}
                    onChangeText={this.handleSearch}
                    value={searchText}
                />
            </View>
        )
        {/* End search component */ }
    }

}