import React, { useState, useEffect } from 'react';
import { baseURL } from '../../api/url';
import { View, Image, ActivityIndicator } from 'react-native';
import Carousel from 'react-native-snap-carousel';

const CarouselWidget = () => {
    const [carouselItems, setCarouselItems] = useState({});
    const [sliderWidth, setSliderWidth] = useState(200);
    const [itemWidth, setItemWidth] = useState(100);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`${baseURL}carousel`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'x-api-key': ''
                    }
                });
                setCarouselItems(await response.json())
            } catch (e) {
                console.log(e);
            }
        }
        fetchData();
    }, [])

    return (
        <View style={{ height: 120 }} onLayout={(event) => {
            const { width } = event.nativeEvent.layout;
            setSliderWidth(width);
            setItemWidth(width);
        }}>
            {renderCarousel(carouselItems.carousel, itemWidth, sliderWidth)}
        </View>
    )
}

const renderCarousel = (carouselItems, sliderWidth, itemWidth) => {
    if (carouselItems) {
        return (
            <Carousel
                autoplay={true}
                loop={true}
                data={carouselItems}
                renderItem={renderCarouselItem}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
            />
        )
    } else {
        return (
            <ActivityIndicator size='large' color='#0000ff' />
        );
    }
}

const renderCarouselItem = ({ item, index }) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
            <Image
                style={{ width: null, height: 120, flex: 1, borderRadius: 7 }}
                source={{ uri: item.uri }}
            />
        </View>
    );
}

export default CarouselWidget;