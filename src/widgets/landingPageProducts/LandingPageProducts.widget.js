import React, { useState, useEffect } from 'react';

import { View, Text, Image, FlatList, TouchableOpacity } from 'react-native';

import colors from '../../styles/colors.default';
import { baseURL } from '../../api/url';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { requestGetLandingPageProducts } from './LandingPageProducts.actions';

class LandingPageProductsWidget extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    componentDidMount() {
        this.landingPageProducts;
    }

    handleRefresh = () => {
        this.landingPageProducts;
    }

    get landingPageProducts() {
        this.props.requestGetLandingPageProducts();
    }

    renderLandingPageProductItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                style={{ backgroundColor: '#ffffff', flex: 1, height: 240, borderRadius: 5, marginRight: 10, marginBottom: 10 }}
                onPress={() => this.props.navigation.navigate('productOverview', { product: item })} >
                <Image
                    style={{ width: null, height: 148, borderTopRightRadius: 5, borderTopLeftRadius: 5 }}
                    source={{ uri: item.multimedia.images.length?item.multimedia.images.filter(image => image.featured)[0].uri:'https://uwosh.edu/facilities/wp-content/uploads/sites/105/2018/09/no-photo.png' }}
                />
                <View style={{ flex:1, padding: 10 }}>
                    <View>
                        <Text numberOfLines={2}>{item.metadata.name}</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', paddingTop: 10 }}>
                        <Text>{item.price.currency}</Text>
                        <Text style={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid', color: 'gray' }}>{item.price.actualPrice}</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 22, marginVertical: -5 }}>{item.price.currentSellPrice}</Text>
                    </View>
                    <View style={{ paddingTop: 15, flex: 1, flexDirection: 'row', justifyContent:'space-between' }}>
                        <View style={{ backgroundColor: colors.activeTintColor, borderRadius: 3, alignItems: 'center', width: 60 }}>
                            <Text style={{ fontSize: 12, color: colors.white, fontWeight: 'bold' }}>{`${item.discount.discountPercent}% off`}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    get renderLandingPageProducts() {
        if (this.props.landingPageProducts) {
            return (
                <FlatList
                    contentContainerStyle={{ paddingVertical: 20 }}
                    data={this.props.landingPageProducts}
                    keyExtractor={product => product.id}
                    renderItem={this.renderLandingPageProductItem}
                    onRefresh={() => this.handleRefresh()}
                    refreshing={this.state.loading}
                    numColumns={2}
                />
            )
        }
    }

    render() {
        {/* Todo extract landing page products */ }
        return (
            <View style={{ padding: 10, flex: 1 }}>
                <View>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: colors.black }}>More To Love</Text>
                </View>
                <View>
                    {this.renderLandingPageProducts}
                </View>
            </View>
        )
        {/* End of landing page products */ }
    }

}

const mapStateToProps = (store) => {
    return {
        landingPageProducts: store.landingPageProducts
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetLandingPageProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LandingPageProductsWidget);