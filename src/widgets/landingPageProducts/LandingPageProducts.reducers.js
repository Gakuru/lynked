import { GET_LANDING_PAGE_PRODUCTS_SUCCESS } from "./LandingPageProducts.types";

export const landingPageProducts = (state = null, { type, landingPageProducts }) => {
    switch (type) {
        case GET_LANDING_PAGE_PRODUCTS_SUCCESS:
            {
                return landingPageProducts
            }
        default:
            {
                return state
            }
    }
}