import { put, call, takeEvery, all } from 'redux-saga/effects';
import { REQUEST_GET_LANDING_PAGE_PRODUCTS } from './LandingPageProducts.types';
import { getLandingPageProductsSuccess } from './LandingPageProducts.actions';

const __getLandingPageProducts = async () => {
    try {
        const response = await fetch(`http://localhost:8030/api/v1/landingpageproducts`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            }
        });
        return await response ? response.json() : null;
    } catch (e) {
        console.log(e);
    }
}

function* getLandingPageProducts() {
    try {
        const data = yield call(__getLandingPageProducts);
        yield put(getLandingPageProductsSuccess(data.landingPageProducts));
    } catch (err) {
        console.log(err);
    }
}

export default function* landingPageProducts() {
    yield all([
        takeEvery(REQUEST_GET_LANDING_PAGE_PRODUCTS, getLandingPageProducts),
    ]);
}