import {
    REQUEST_GET_LANDING_PAGE_PRODUCTS,
    GET_LANDING_PAGE_PRODUCTS_SUCCESS
} from "./LandingPageProducts.types";

export const requestGetLandingPageProducts = () => ({ type: REQUEST_GET_LANDING_PAGE_PRODUCTS })
export const getLandingPageProductsSuccess = landingPageProducts => ({ type: GET_LANDING_PAGE_PRODUCTS_SUCCESS, landingPageProducts })