import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { View, Image, Text, TouchableOpacity } from 'react-native';
import styles from '../../../styles/colors.default';

class ShopHeaderWidget extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            shop: props.shop,
            rating: props.rating !== undefined ? props.rating : true,
            deliveriesCount: props.deliveriesCount !== undefined ? props.deliveriesCount : true,
            userResourceControl: props.userResourceControl,
        }
    }

    componentDidMount() {
        // this.props.requestGetShopProducts();
        // console.log(this.state.userResourceControl);
    }

    renderSeparator = index => {
        if (index < this.props.shopProducts.length - 1)
            return (
                <View style={styles.separator} />
            )
    }

    renderRating = () => {
        if (this.state.rating) {
            return (
                <Text style={{ position: 'absolute', left: 0, top: 10 }}>{`${this.state.shop.analysis.starRatingAverage} star rating`}</Text>
            )
        }
    }

    renderDeliveriesCount = () => {
        if (this.state.deliveriesCount) {
            return (
                <Text style={{ position: 'absolute', right: 0, top: 10 }}>{`${this.state.shop.analysis.deliveriesCount} + deliveries`}</Text>
            )
        }
    }

    renderShopHeader = () => {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('shopCreate', { shop: this.state.shop })}>

                <View style={{ padding: 10 }}>
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ width: 88, height: 88, borderRadius: 50 }}>
                            <Image
                                style={{ width: null, height: null, resizeMode: 'contain', flex: 1, borderRadius: 50 }}
                                source={{ uri: this.state.shop.multimedia.logoUri }}
                            />
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#f7f7f7', marginTop: -48 }}>
                        {this.renderRating()}
                        {this.renderDeliveriesCount()}
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 52 }}>
                        <Text style={{ fontFamily: 'System', fontSize: 22, fontWeight: '200' }}>{this.state.shop.category.name}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={{}}>
                {this.renderShopHeader()}
            </View>
        )
    }

}

const mapStateToProps = (store) => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShopHeaderWidget);