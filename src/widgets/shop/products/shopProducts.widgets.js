import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { View, Image, Text, StyleSheet, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import styles from '../../../styles/style.default';

import { requestGetShopProducts } from './shopProducts.actions';

const productDivisonKeys = {
    service: 'service',
    product: 'product'
}

class ShopProductsWidget extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            shop: props.shop,
            loading: false
        }

        this.props.navigation.addListener('willFocus', () => {
            this.getShopProductsList();
        })
    }

    componentDidMount() {
        this.getShopProductsList();
    }

    getShopProductsList = () => {
        this.props.requestGetShopProducts({ shopId: this.state.shop.id });
    }

    renderSeparator = index => {
        if (index < this.props.shopProducts.length - 1)
            return (
                <View style={styles.separator} />
            )
    }

    renderInStockText = ({ stock, division }) => {
        if (division.key === productDivisonKeys.product)
            return <Text style={{}}>{`In stock ${stock.stockCount}`}</Text>
    }

    renderProductCategoryText = ({ category, division }) => {
        if (division.key === productDivisonKeys.service)
            return <Text style={{ fontSize: 11 }}>{category.name}</Text>
    }

    renderShopProducts = () => {
        if (this.props.shopProducts) {
            return (
                <View>

                    <View style={styles.separator}></View>

                    <View style={{ paddingTop: 15 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 20, fontWeight: '100' }}>Merchandise / Services</Text>
                            <Button
                                buttonStyle={{ backgroundColor: 'transparent' }}
                                onPress={() => this.props.navigation.navigate('shopProductCreate', { product: null, shop: this.state.shop })}
                                icon={
                                    <Icon
                                        type='feather'
                                        name="plus-circle"
                                        size={28}
                                        color="#0275d8"
                                        iconStyle={{ marginVertical: -10 }}
                                    />
                                }
                            />
                        </View>
                    </View>

                    <View style={{ paddingTop: 15 }}>
                        <FlatList
                            data={this.props.shopProducts}
                            keyExtractor={shopProducts => shopProducts.id}
                            renderItem={({ item: product, index }) => {
                                return (
                                    <View>
                                        <TouchableOpacity
                                            style={{ flex: 1, flexDirection: 'row', marginBottom: 5, marginTop: 5 }}
                                            onPress={() => this.props.navigation.navigate('shopProductCreate', { product, shop: this.state.shop })}>
                                            <FlatList
                                                style={{ width: 140, height: 140 }}
                                                data={product.multimedia.images}
                                                keyExtractor={images => images.id}
                                                onRefresh={() => this.getShopProductsList()}
                                                refreshing={this.state.loading}
                                                numColumns={2}
                                                renderItem={({ item: image }) => {
                                                    return (
                                                        <View style={{ width: 64, height: 64, backgroundColor: '#f0f0f0', justifyContent: 'center', alignItems: 'center' }}>
                                                            <View style={{ width: 52, height: 52 }}>
                                                                <Image
                                                                    style={{ width: null, height: null, resizeMode: 'contain', flex: 1 }}
                                                                    source={{ uri: image.uri }}
                                                                />
                                                            </View>
                                                        </View>
                                                    )
                                                }}
                                            />
                                            <View style={{ padding: 5, flex: 1 }}>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={{ fontSize: 18, flexWrap: 'wrap' }}>{product.metadata.name}</Text>
                                                </View>

                                                <View style={{}}>
                                                    <Text style={{ fontSize: 13 }}>{product.division.name}</Text>
                                                    {this.renderProductCategoryText(product)}
                                                </View>

                                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                                                    {this.renderInStockText(product)}
                                                    <Text style={{}}>{`Discount ${product.discount.discountPercent.toString()}%`}</Text>
                                                </View>

                                                <View style={{ position: 'absolute', bottom: 0, left: 5 }}>
                                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                                        <Text>{product.price.currency}</Text>
                                                        <Text style={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid', color: 'gray' }}>{product.price.actualPrice.toString()}</Text>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 22, marginVertical: -5 }}>{product.price.currentSellPrice.toString()}</Text>
                                                    </View>
                                                </View>

                                            </View>

                                        </TouchableOpacity>

                                        {this.renderSeparator(index)}

                                    </View>
                                )
                            }}
                        />
                    </View>
                </View>
            )
        }
    }

    render() {
        return (
            <View style={{}}>
                {this.renderShopProducts()}
            </View>
        )
    }

}

const mapStateToProps = (store) => {
    return {
        shopProducts: store.shopProducts
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    requestGetShopProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShopProductsWidget);