import {
    REQUEST_GET_SHOP_PRODUCTS,
    GET_SHOP_PRODUCTS_SUCCESS,
    REQUEST_CREATE_SHOP_PRODUCT,
    CREATE_SHOP_PRODUCT_SUCCESS,
    REQUEST_REMOVE_SHOP_PRODUCT,
    REMOVE_SHOP_PRODUCT_SUCCESS
} from "./shopProducts.types";

export const requestGetShopProducts = (shopId) => ({ type: REQUEST_GET_SHOP_PRODUCTS,shopId })
export const getShopProductsSuccess = (shopProducts) => ({ type: GET_SHOP_PRODUCTS_SUCCESS, shopProducts })

export const requestCreateShopProduct = (params) => ({ type: REQUEST_CREATE_SHOP_PRODUCT, params })
export const createShopProductSuccess = (shopProduct) => ({ type: CREATE_SHOP_PRODUCT_SUCCESS, shopProduct })

export const requestRemoveShopProduct = (product) => ({type: REQUEST_REMOVE_SHOP_PRODUCT, product})
export const removeShopProductSuccess = (shopProduct) => ({type: REMOVE_SHOP_PRODUCT_SUCCESS, shopProduct})