import { put, call, takeEvery, all } from 'redux-saga/effects';
import { REQUEST_GET_SHOP_PRODUCTS, REQUEST_CREATE_SHOP_PRODUCT, REQUEST_REMOVE_SHOP_PRODUCT } from './shopProducts.types';
import { getShopProductsSuccess, createShopProductSuccess, removeShopProductSuccess } from './shopProducts.actions';
import { baseURL } from '../../../api/url';

const __getShopProducts = async ({ shopId }) => {
    try {
        const response = await fetch(`${baseURL}shops/${shopId}/products`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            }
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

const __createShopProduct = async ({ shopProduct, shopId, id }) => {
    try {
        const response = await fetch(`${baseURL}shops/${shopId}/products`, {
            method: id ? `PATCH` : `POST`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            },
            body: shopProduct
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

const __removeShopProduct = async ({ product }) => {
    try {
        const response = await fetch(`${baseURL}shops/${product.shop.id}/products`, {
            method: `DELETE`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': ''
            },
            body: JSON.stringify(product)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

function* getShopProducts({ shopId }) {
    try {
        const data = yield call(__getShopProducts, shopId);
        yield put(getShopProductsSuccess(data.shopProducts));
    } catch (err) {
        console.log(err);
    }
}

function* createShopProduct({ params }) {
    try {
        const data = yield call(__createShopProduct, params);
        yield put(createShopProductSuccess(data));
    } catch (err) {
        console.log(err);
    }
}

function* removeShopProduct({ product }) {
    try {
        const data = yield call(__removeShopProduct, product);
        yield put(removeShopProductSuccess(data));
    } catch (err) {
        console.log(err);
    }
}

export default function* shopProductsSaga() {
    yield all([
        takeEvery(REQUEST_GET_SHOP_PRODUCTS, getShopProducts),
        takeEvery(REQUEST_CREATE_SHOP_PRODUCT, createShopProduct),
        takeEvery(REQUEST_REMOVE_SHOP_PRODUCT, removeShopProduct),
    ]);
}