import {
    GET_SHOP_PRODUCTS_SUCCESS,
    CREATE_SHOP_PRODUCT_SUCCESS,
    REMOVE_SHOP_PRODUCT_SUCCESS
} from "./shopProducts.types";

export const shopProducts = (state = null, { type, shopProducts }) => {
    switch (type) {
        case GET_SHOP_PRODUCTS_SUCCESS:
            {
                return shopProducts
            }
        default:
            {
                return state
            }
    }
}

export const shopProduct = (state = null, { type, shopProduct }) => {
    switch (type) {
        case CREATE_SHOP_PRODUCT_SUCCESS:
            {
                return shopProduct
            }
        case REMOVE_SHOP_PRODUCT_SUCCESS:
            {
                return shopProduct
            }
        default:
            {
                return state
            }
    }
}