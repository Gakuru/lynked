import React from 'react';
import { AppState, Platform } from 'react-native';
import PushNotification from 'react-native-push-notification';

export default class PushController extends React.Component {
    constructor(props) {
        super(props);

        PushNotification.configure({
            onNotification: (notification) => {
                console.log('NOTIFICATION:', notification)
            }
        })
    }

    render() {
        return null;
    }
}
