import React, { Component } from 'react';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
MaterialIcons.loadFont();

import Feather from 'react-native-vector-icons/Feather'
Feather.loadFont();

import AntDesign from 'react-native-vector-icons/AntDesign'
AntDesign.loadFont();

import { Provider } from 'react-redux';
import store from './store';
import BottomNavigation from './src/navigation/RootNavigation';
import PushController from './src/services/pushController';

import { AppState } from 'react-native';
import PushNotification from 'react-native-push-notification';

import RNFirebase from 'react-native-firebase';

const configurationOptions = {
  debug: true,
  promptOnMissingPlayServices: true
}

const firebase = RNFirebase.initializeApp(configurationOptions);


export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      notifications: {
        hasNotifications: true
      },
      seconds: 5,
    };
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);

    firebase.messaging().getToken().then((token) => {
      console.log('token: ',token);
    });

    firebase.messaging().onTokenRefresh((token) => {
      console.log('refresh token: ',token);
    });

  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = (appState) => {
    if (appState === 'background' && this.state.notifications.hasNotifications) {
      const hasNotification = true;
      if (hasNotification) {
        let date = new Date(Date.now() + (this.state.seconds * 1000));

        PushNotification.localNotificationSchedule({
          message: "My Notification Message",
          date,
        });
      }
    }
  }

  render() {
    return (
      <Provider store={store}>
        <BottomNavigation />
        <PushController />
      </Provider>
    )
  }
}