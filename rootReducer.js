import {
    combineReducers
} from 'redux';

import { landingPageCategories } from './src/widgets/landingPageCategories/LandingPageCategories.reducers';
import { landingPageProducts } from './src/widgets/landingPageProducts/LandingPageProducts.reducers';
import { shopProducts, shopProduct } from './src/widgets/shop/products/shopProducts.reducers';
import { shops, shop } from './src/widgets/shops/shops.reducers';
import { productOverview } from './src/screens/products/product.reducer';
import { user } from './src/screens/auth/user.reducer';

export default combineReducers({
    landingPageCategories,
    landingPageProducts,
    shopProducts,
    shopProduct,
    shops,
    shop,
    productOverview,
    user
});